package RFCavity;

import fr.esrf.tangoatk.core.AttributeList;
import fr.esrf.tangoatk.core.CommandList;
import fr.esrf.tangoatk.core.ConnectionException;
import fr.esrf.tangoatk.core.ICommand;
import fr.esrf.tangoatk.core.attribute.EnumScalar;
import fr.esrf.tangoatk.core.attribute.NumberScalar;
import fr.esrf.tangoatk.core.attribute.StringScalar;
import fr.esrf.tangoatk.core.command.VoidVoidCommand;
import fr.esrf.tangoatk.widget.attribute.EnumScalarComboEditor;
import fr.esrf.tangoatk.widget.attribute.NumberScalarWheelEditor;
import fr.esrf.tangoatk.widget.attribute.SimpleScalarViewer;
import fr.esrf.tangoatk.widget.attribute.StatusViewer;
import fr.esrf.tangoatk.widget.command.VoidVoidCommandViewer;
import fr.esrf.tangoatk.widget.util.ATKConstant;
import fr.esrf.tangoatk.widget.util.ATKGraphicsUtils;
import fr.esrf.tangoatk.widget.util.ErrorHistory;
import fr.esrf.tangoatk.widget.util.ErrorPopup;


import javax.swing.*;
import javax.swing.border.TitledBorder;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;

/**
 * Regulation loop panel
 */
public class LoopPanel extends JFrame implements ActionListener {

  static ErrorHistory errWin=null;
  
  private final static Font borderFont = new Font("Dialog",Font.BOLD,12);
  private final static Font viewerFont = new Font("Dialog",Font.BOLD,14);
  private final static Font setterFont = new Font("Lucida Bright", Font.PLAIN, 14);
  
  // Tuning loop encoder info panel
  class EncoderInfoPanel extends JPanel {

    JLabel encoderLabel;
    SimpleScalarViewer encoderPos;
    JLabel sourceLabel;
    EnumScalarComboEditor positionSource;
    NumberScalar posModel = null;
    EnumScalar sourceModel = null;
    
    EncoderInfoPanel(String devName,String prefix) {
        
      setLayout(null);
      encoderLabel = new JLabel("Encoder"+prefix);
      encoderLabel.setFont(ATKConstant.labelFont);
      encoderPos = new  SimpleScalarViewer();
      encoderPos.setFont(ATKConstant.labelFont);
      encoderPos.setBackground(innerPanel.getBackground());
      encoderPos.setAlarmEnabled(false);
      encoderPos.setBorder(BorderFactory.createLoweredBevelBorder());
      sourceLabel = new JLabel("Pos. Source"+prefix);
      sourceLabel.setFont(ATKConstant.labelFont);
      positionSource = new EnumScalarComboEditor();
      positionSource.setFont(ATKConstant.labelFont);
      add(encoderLabel);
      add(encoderPos);
      add(sourceLabel);
      add(positionSource);
      encoderLabel.setBounds(2,2,60,25);
      encoderPos.setBounds(65,2,96,25);
      sourceLabel.setBounds(165,2,90,25);
      positionSource.setBounds(258,2,110,25);
      
      try {
        posModel = (NumberScalar)attList.add(devName+"/EncoderPosition"+prefix);
        encoderPos.setModel(posModel);     
        sourceModel = (EnumScalar)attList.add(devName+"/PositionSource"+prefix);
        positionSource.setEnumModel(sourceModel);          
      } catch( ConnectionException e ) {
        System.out.println(e.getErrors()[0].desc);
      }
      
    }
    
    void clearModel() {
      encoderPos.clearModel();
      positionSource.setEnumModel(null);
    }
    
  }

  private String devName;
  private String loopType;

  private JPanel cmdPanel;
  private JPanel attPanel;
  private JPanel innerPanel;
  private JPanel upPanel;
  private JPanel downPanel;

  private JButton dismissBtn;

  private StatusViewer statusViewer = null;

  private JLabel                    setLabel = null;
  private SimpleScalarViewer        setViewer = null;
  private NumberScalarWheelEditor   setSetter = null;

  private JLabel                    set2Label = null;
  private SimpleScalarViewer        set2Viewer = null;
  private NumberScalarWheelEditor   set2Setter = null;

  private JLabel                    set3Label = null;
  private SimpleScalarViewer        set3Viewer = null;
  private NumberScalarWheelEditor   set3Setter = null;

  private JLabel                    set4Label = null;
  private SimpleScalarViewer        set4Viewer = null;
  private NumberScalarWheelEditor   set4Setter = null;
  
  private EncoderInfoPanel          enc1 = null;
  private EncoderInfoPanel          enc2 = null;
  
  private VoidVoidCommandViewer     openCmd = null;
  private VoidVoidCommand           openCommand = null;
  private VoidVoidCommandViewer     closeCmd = null;
  private VoidVoidCommand           closeCommand = null;
  private JButton                   expertBtn = null;
  private JButton                   expertMotorBtn = null;
  private JButton                   expertMotor2Btn = null;
  private boolean                   hasMotor = false;
  private boolean                   hasMotor2 = false;
  private String                    motorDevName = null;
  private String                    motor2DevName = null;

  private AttributeList attList;
  private CommandList cmdList;
  public ErrorHistory errorWin;

  public LoopPanel() {
  }

  public LoopPanel(String loopType) {
    errorWin = errWin;
    initLoopPanel(loopType);
  }

  public void initLoopPanel(String loopType) {
    
    addWindowListener(new WindowAdapter() {
      public void windowClosing(WindowEvent e) {
         exitPanel();
      }
    });

    String[] params = loopType.split(",");
    if( params.length!=2 ) {
      JOptionPane.showMessageDialog(this,"Invalid argument: Type,TraDeviceNameSuffix expected.","LoopPanel Error",JOptionPane.ERROR_MESSAGE);
      return;
    }

    this.loopType = params[0];

    String title;

    String setpoint1AttName=null;
    String viewpoint1AttName=null;
    String setpoint1Label=null;

    String setpoint2AttName=null;
    String viewpoint2AttName=null;
    String setpoint2Label=null;

    String setpoint3AttName=null;
    String viewpoint3AttName=null;
    String setpoint3Label=null;

    String setpoint4AttName=null;
    String viewpoint4AttName=null;
    String setpoint4Label=null;

    String openStr;
    String closeStr;
    int vSize = 0;

    if( this.loopType.equalsIgnoreCase("mag") ) {

      // Magnitude Loop (only cav11,12,13)
      devName = "srrf/ssa-driver/" + params[1];
      String cavName = "cav1" + params[1].substring(params[1].length()-1);

      title = "Cavity Voltage Loop";

      setpoint1AttName = devName + "/Amplitude";
      viewpoint1AttName = devName + "/Amplitude";
      setpoint1Label = "Pin Att.";

      setpoint2AttName = devName + "/RFAmplitude";
      viewpoint2AttName = "srrf/cav-adc/" + cavName + "/Cavity_Voltage";
      setpoint2Label = "RF Amplitude";

      openStr = "OpenMagLoop";
      closeStr = "CloseMagLoop";

    } else if( this.loopType.equalsIgnoreCase("magsy") ) {

      devName = "sy/rfssa-driver/" + params[1];

      title = "SY Cavity Voltage Loop";

      setpoint1AttName = devName + "/Amplitude";
      //viewpoint1AttName = devName + "/Amplitude";
      setpoint1Label = "Pin Att Inj (Open loop)";

      setpoint2AttName = devName + "/WaveformAmplitude";
      viewpoint2AttName = devName + "/WaveformAmplitude";
      setpoint2Label = "Pin Att Ext (Open loop)";

      setpoint3AttName = devName + "/RFAmplitude";
      viewpoint3AttName = devName + "/CavInjVoltage";
      setpoint3Label = "Cav Voltage (Inj)";

      setpoint4AttName = devName +"/RFWaveformAmplitude";
      viewpoint4AttName = devName +"/CavVoltage";
      setpoint4Label = "Cav Voltage (Ext)";

      openStr = "OpenMagLoop";
      closeStr = "CloseMagLoop";

      vSize = 90;

    } else if ( this.loopType.equalsIgnoreCase("phase") ) {

      // Phase Loop (only cav11,12,13)
      devName = "srrf/ssa-driver/" + params[1];
      String cavName = "cav1" + params[1].substring(params[1].length()-1);

      title = "Cavity Phase Loop";

      setpoint1AttName = devName + "/Phase";
      viewpoint1AttName = devName + "/Phase";
      setpoint1Label = "Phase shifter";

      setpoint2AttName = devName + "/RFPhase";
      viewpoint2AttName = "srrf/cav-adc/" + cavName + "/Phase_Cavity_1";
      setpoint2Label = "RF Phase";

      openStr = "OpenPhaseLoop";
      closeStr = "ClosePhaseLoop";

    } else if ( this.loopType.equalsIgnoreCase("phasesy") ) {

      devName = "sy/rfssa-driver/" + params[1];
      String cavName = "cav" + params[1].substring(params[1].length()-1);

      title = "SY Cavity Phase Loop";

      setpoint1AttName = devName + "/Phase";
      viewpoint1AttName = devName + "/Phase";
      setpoint1Label = "Phase shifter";

      setpoint2AttName = devName + "/RFPhase";
      viewpoint2AttName = "sy/rfcav-adc/" + cavName + "/Phase_Cavity_1";
      setpoint2Label = "RF Phase";

      openStr = "OpenPhaseLoop";
      closeStr = "ClosePhaseLoop";

    } else if ( this.loopType.equalsIgnoreCase("tuning") ) {

      devName = "srrf/cav-tunerloop/" + params[1];

      title = "Cavity Tuning Loop";

      setpoint1AttName = devName + "/TuningAngle";
      viewpoint1AttName = devName + "/TuningAngle";
      setpoint1Label = "Tuning angle";

      setpoint2AttName = devName + "/" + "MotorPosition";
      viewpoint2AttName = devName + "/" + "MotorPosition";
      setpoint2Label = "Motor Position";

      openStr = "Open";
      closeStr = "Close";
      hasMotor = true;
      motorDevName = "srrf/cav-motor/" + params[1];

    } else if (this.loopType.equalsIgnoreCase("tuningsy")) {

      devName = "sy/rfcav-tunerloop/" + params[1];

      title = "SY Cavity Tuning Loop";

      openStr = "Open";
      closeStr = "Close";
      hasMotor = true;
      hasMotor2 = true;
      motorDevName = "sy/rfcav-motor/" + params[1] + "-1";
      motor2DevName = "sy/rfcav-motor/" + params[1] + "-2";

      setpoint1AttName = devName + "/TuningAngle";
      viewpoint1AttName = setpoint1AttName;
      setpoint1Label = "Tuning angle";

      setpoint2AttName = devName + "/MotorPosition";
      viewpoint2AttName = setpoint2AttName;
      setpoint2Label = "Motor1 Position";

      setpoint3AttName = devName + "/MotorPosition2";
      viewpoint3AttName = setpoint3AttName;
      setpoint3Label = "Motor2 Position";
      vSize = 40;

    } else {

      JOptionPane.showMessageDialog(this,"Invalid argument: loop type "+this.loopType+" undefined","LoopPanel Error",JOptionPane.ERROR_MESSAGE);
      return;

    }

    innerPanel = new JPanel();
    innerPanel.setLayout(new BorderLayout());
    upPanel = new JPanel();
    upPanel.setLayout(new BorderLayout());

    int hSize = 160;

    statusViewer = new StatusViewer();
    statusViewer.setPreferredSize(new Dimension(260,hSize));
    upPanel.add(statusViewer,BorderLayout.CENTER);

    cmdPanel = new JPanel();
    cmdPanel.setLayout(null);
    cmdPanel.setPreferredSize(new Dimension(120,hSize));
    upPanel.add(cmdPanel,BorderLayout.EAST);

    innerPanel.add(upPanel,BorderLayout.NORTH);

    openCmd = new VoidVoidCommandViewer();
    openCmd.setExtendedParam("text", "Open", false);
    openCmd.setBounds(10,10,100,25);
    cmdPanel.add(openCmd);

    closeCmd = new VoidVoidCommandViewer();
    closeCmd.setExtendedParam("text", "Close", false);
    closeCmd.setBounds(10,40,100,25);
    cmdPanel.add(closeCmd);

    expertBtn = new JButton("Expert");
    expertBtn.addActionListener(this);
    expertBtn.setBounds(10,70,100,25);
    cmdPanel.add(expertBtn);

    int mSize = 0;
    if( hasMotor ) {
      expertMotorBtn = new JButton("Motor #1");
      expertMotorBtn.addActionListener(this);
      expertMotorBtn.setBounds(10,100,100,25);
      cmdPanel.add(expertMotorBtn);
      mSize += 30;
    }

    if( hasMotor2 ) {
      expertMotor2Btn = new JButton("Motor #2");
      expertMotor2Btn.addActionListener(this);
      expertMotor2Btn.setBounds(10,130,100,25);
      cmdPanel.add(expertMotor2Btn);
      mSize += 30;
    }

    attPanel = new JPanel();
    attPanel.setLayout(null);
    attPanel.setBorder(BorderFactory.createTitledBorder(BorderFactory.createEtchedBorder(), title,
            TitledBorder.LEFT, TitledBorder.DEFAULT_POSITION,
            borderFont, Color.BLACK));

    attPanel.setPreferredSize(new Dimension(380,105+vSize+mSize));
    innerPanel.add(attPanel,BorderLayout.CENTER);

    setLabel = new JLabel(setpoint1Label);
    setLabel.setFont(ATKConstant.labelFont);
    setLabel.setBounds(10,20,150,30);
    attPanel.add(setLabel);
    setViewer = new SimpleScalarViewer();
    setViewer.setFont(viewerFont);
    setViewer.setBackground(innerPanel.getBackground());
    setViewer.setAlarmEnabled(false);
    setViewer.setBorder(BorderFactory.createLoweredBevelBorder());
    setViewer.setBounds(160,18,100,35);
    if(viewpoint1AttName!=null) attPanel.add(setViewer);
    setSetter = new NumberScalarWheelEditor();
    setSetter.setBackground(innerPanel.getBackground());
    setSetter.setFont(setterFont);
    setSetter.setBounds(260,15,110,40);
    attPanel.add(setSetter);

    set2Label = new JLabel(setpoint2Label);
    set2Label.setFont(ATKConstant.labelFont);
    set2Label.setBounds(10, 60, 150, 30);
    attPanel.add(set2Label);
    set2Viewer = new SimpleScalarViewer();
    set2Viewer.setFont(viewerFont);
    set2Viewer.setBackground(innerPanel.getBackground());
    set2Viewer.setAlarmEnabled(false);
    set2Viewer.setBorder(BorderFactory.createLoweredBevelBorder());
    set2Viewer.setBounds(160, 58, 100, 35);
    attPanel.add(set2Viewer);
    set2Setter = new NumberScalarWheelEditor();
    set2Setter.setBackground(innerPanel.getBackground());
    set2Setter.setFont(setterFont);
    set2Setter.setBounds(260, 55, 110, 40);
    attPanel.add(set2Setter);

    if(vSize>0) {

      set3Label = new JLabel(setpoint3Label);
      set3Label.setFont(ATKConstant.labelFont);
      set3Label.setBounds(10, 100, 150, 30);
      attPanel.add(set3Label);
      set3Viewer = new SimpleScalarViewer();
      set3Viewer.setFont(viewerFont);
      set3Viewer.setBackground(innerPanel.getBackground());
      set3Viewer.setAlarmEnabled(false);
      set3Viewer.setBorder(BorderFactory.createLoweredBevelBorder());
      set3Viewer.setBounds(160, 98, 100, 35);
      attPanel.add(set3Viewer);
      set3Setter = new NumberScalarWheelEditor();
      set3Setter.setBackground(innerPanel.getBackground());
      set3Setter.setFont(setterFont);
      set3Setter.setBounds(260, 95, 110, 40);
      attPanel.add(set3Setter);
    }

    if(vSize>40) {

      set4Label = new JLabel(setpoint4Label);
      set4Label.setFont(ATKConstant.labelFont);
      set4Label.setBounds(10, 140, 150, 30);
      attPanel.add(set4Label);
      set4Viewer = new SimpleScalarViewer();
      set4Viewer.setFont(viewerFont);
      set4Viewer.setBackground(innerPanel.getBackground());
      set4Viewer.setAlarmEnabled(false);
      set4Viewer.setBorder(BorderFactory.createLoweredBevelBorder());
      set4Viewer.setBounds(160, 138, 100, 35);
      attPanel.add(set4Viewer);
      set4Setter = new NumberScalarWheelEditor();
      set4Setter.setBackground(innerPanel.getBackground());
      set4Setter.setFont(setterFont);
      set4Setter.setBounds(260, 135, 110, 40);
      attPanel.add(set4Setter);

    }
    
    downPanel = new JPanel();
    downPanel.setLayout(new FlowLayout(FlowLayout.RIGHT));
    innerPanel.add(downPanel,BorderLayout.SOUTH);

    dismissBtn = new JButton("Dismiss");
    dismissBtn.addActionListener(this);
    downPanel.add(dismissBtn);

    // Connect to attribute
    attList = new AttributeList();
    attList.addErrorListener(errorWin);
    attList.addSetErrorListener(ErrorPopup.getInstance());
    cmdList = new CommandList();
    cmdList.addErrorListener(errorWin);
    cmdList.addErrorListener(ErrorPopup.getInstance());

    if( hasMotor ) {        
      enc1 = new EncoderInfoPanel(devName,"");
      attPanel.add(enc1);
      enc1.setBounds(5,100+vSize,370,30);
    }

    if( hasMotor2 ) {        
      enc2 = new EncoderInfoPanel(devName,"2");
      attPanel.add(enc2);
      enc2.setBounds(5,130+vSize,370,30);
    }

    try {

      StringScalar statusModel = (StringScalar)attList.add(devName + "/Status");
      statusViewer.setModel(statusModel);

      openCommand = (VoidVoidCommand)cmdList.add(devName+"/"+openStr);
      openCommand.addErrorListener(ErrorPopup.getInstance());
      openCmd.setModel(openCommand);

      closeCommand = (VoidVoidCommand)cmdList.add(devName+"/"+closeStr);
      closeCommand.addErrorListener(ErrorPopup.getInstance());
      closeCmd.setModel(closeCommand);

      initModel(viewpoint1AttName,setViewer,setpoint1AttName,setSetter);
      initModel(viewpoint2AttName,set2Viewer,setpoint2AttName,set2Setter);
      initModel(viewpoint3AttName,set3Viewer,setpoint3AttName,set3Setter);
      initModel(viewpoint4AttName,set4Viewer,setpoint4AttName,set4Setter);

    } catch( Exception e ) {
      System.out.println(e.getMessage());
    }

    attList.setRefreshInterval(1000);
    attList.startRefresher();

    setContentPane(innerPanel);
    setTitle(this.loopType + "Loop [" + params[1] + "]");
    ATKGraphicsUtils.centerFrameOnScreen(this);
    setVisible(true);

  }
    
  private void initModel(String viewName,SimpleScalarViewer setViewer,
                         String setName,NumberScalarWheelEditor setSetter)
      throws ConnectionException {

    if( viewName!=null ) {
      NumberScalar viewModel = (NumberScalar)attList.add(viewName);
      setViewer.setModel(viewModel);
    }

    if( setName!=null ) {
      NumberScalar setModel = (NumberScalar)attList.add(setName);
      setSetter.setModel(setModel);
    }

  }

  public void actionPerformed(ActionEvent e) {

    Object src = e.getSource();

    if( src==expertBtn ) {

      new atkpanel.MainPanel(devName,false);

    } else if ( src==expertMotorBtn ) {

      new atkpanel.MainPanel(motorDevName,false);

    } else if ( src==expertMotor2Btn ) {

      new atkpanel.MainPanel(motor2DevName,false);

    } else if ( src==dismissBtn ) {

      dispose();
      setVisible(false);

    }

  }

  private void exitPanel() {

    if(statusViewer!=null) statusViewer.setModel(null);
    if(openCmd!=null) openCmd.setModel((ICommand)null);
    if(openCommand!=null) openCommand.removeErrorListener(ErrorPopup.getInstance());
    if(closeCmd!=null) closeCmd.setModel((ICommand)null);
    if(closeCommand!=null) closeCommand.removeErrorListener(ErrorPopup.getInstance());

    if(setViewer!=null) setViewer.clearModel();
    if(setSetter!=null) setSetter.setModel(null);

    if(set2Viewer !=null) set2Viewer.clearModel();
    if(set2Setter !=null) set2Setter.setModel(null);

    if(set3Viewer !=null) set3Viewer.clearModel();
    if(set3Setter !=null) set3Setter.setModel(null);

    if(set4Viewer !=null) set4Viewer.clearModel();
    if(set4Setter !=null) set4Setter.setModel(null);
    
    if(enc1!=null) enc1.clearModel();
    if(enc2!=null) enc2.clearModel();

    attList.stopRefresher();
    attList.removeErrorListener(errorWin);
    attList.clear();
    attList = null;
    cmdList.removeErrorListener(errorWin);
    cmdList.clear();
    cmdList = null;

    dispose();

  }

  public static void main(String[] args) {

    if(args.length!=1) {
      System.out.println("Usage: looppanel looptype");
      System.exit(-1);
    }
    new LoopPanel(args[0]);

  }

}
